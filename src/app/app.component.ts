import {Component, OnInit} from '@angular/core';

import * as firebase from 'firebase/app';
import * as firebaseui from 'firebaseui';

@Component({
  selector: 'yrm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  ngOnInit(): void {

    const firebaseConfig = {
      apiKey: 'AIzaSyDxUacuKLrTUAroPszuZg8qoYEnOO-2wdA',
      authDomain: 'yrmotion-67437.firebaseapp.com',
      databaseURL: 'https://yrmotion-67437.firebaseio.com',
      projectId: 'yrmotion-67437',
      storageBucket: 'yrmotion-67437.appspot.com',
      messagingSenderId: '259895600803',
      appId: '1:259895600803:web:40af330f26ba28d283c116',
      measurementId: 'G-5BBS8H7HGM'
    };

    firebase.initializeApp(firebaseConfig);

    const ui = new firebaseui.auth.AuthUI(firebase.auth());

    ui.start('#firebaseui-auth-container', {
      signInOptions: [
        firebase.auth.EmailAuthProvider.PROVIDER_ID
      ],
      signInFlow: 'popup',
      callbacks: {
        signInSuccessWithAuthResult(authResult, redirectUrl) {
          // User successfully signed in.
          // Return type determines whether we continue the redirect automatically
          // or whether we leave that to developer to handle.
          document.getElementById('app-wrapper').style.display = 'flex';
          return true;
        },
        uiShown() {
          // The widget is rendered.
          // Hide the loader.
        }
      },
    });

  }

}
